import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import crono from 'vue-crono'
import { BootstrapVue, BootstrapVueIcons } from 'bootstrap-vue'
import FunctionalCalendar from 'vue-functional-calendar'

import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'

Vue.config.productionTip = false

Vue.use(crono)
Vue.use(BootstrapVue)
Vue.use(BootstrapVueIcons)
Vue.use(FunctionalCalendar, {
  dayNames: ['Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa', 'Do'],
  monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre']
})


new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
