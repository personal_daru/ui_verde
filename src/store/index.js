import Vue from 'vue'
import Vuex from 'vuex'
import router from '@/router'
import axios from 'axios'
import VuexPersist from 'vuex-persist'
import { minix, pregunta } from '../components/functions/alertas'
import { io } from 'socket.io-client'

Vue.use(Vuex)

const vuexPersist = new VuexPersist({
  key: 'rhomio',
  storage: window.localStorage,
  reducer: state => ({
    token_sesion: state.token_sesion
  }) 
})

function showAlert(message, callback) {
  alert(message);
  if (callback && typeof callback === 'function') {
    callback();
  }
}

export default new Vuex.Store({
  state: {
    //STATE
    token_sesion:'',
    preferencias: {
      IP: process.env.NODE_ENV == 'production' ? '' : 'http://localhost:1337' ,
      PUERTO: 8005
    },
    load_tiempo: false,
    data_usuario: {},
    role: '',
    rutas:[
    ]
  },
  getters: {
    token_sesion_formateado: state =>{
      let token_formateado = {
        headers:{
          Authorization: `Bearer ${state.token_sesion}`
        }
      }

      return token_formateado
    }
  },
  mutations: {
    //SISTEMA
    set_token_sesion(state, data){
      state.token_sesion = data
    },
    set_load_tiempo(state, data){
      state.load_tiempo = data
    },
    set_data_usuario(state, data){
      state.data_usuario = data
    },
    set_role(state, data){
      state.role = data
    },

    //PRODUCCION
    
  },
  actions: {

    async guardar_data({commit, state}, data){
      try {
        const config = {
          method: 'post',
          url: `${state.preferencias.IP}/api/${data.api}`,
          headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${state.token_sesion}`
          },
          data: data.api == 'auth/local/register' ? data.formulario : { data: data.formulario }
        }

        const r = await axios(config)

        if (r.status == 200) {
          if(r.data.message){
            minix({icon: 'info', mensaje: r.data.message, tiempo: 6000})
          }else{
            if (r.data.mensaje) {
              
              minix({icon: 'success', mensaje: r.data.mensaje.alerta, tiempo: 3000}) 
            }else{
              minix({icon: 'success', mensaje: 'GUARDADO CON EXITO', tiempo: 3000}) 
            }

            return r.data
          }

        }else{
          minix({icon: 'info', mensaje: 'HUBO UN ERROR AL GUARDAR', tiempo: 6000})
        }

      } catch (error) {
        if (error.response.data.error.message) {
          minix({icon: 'error', mensaje: error.response.data.error.message, tiempo: 6000})
        }else{
          minix({icon: 'error', mensaje: error.message, tiempo: 3000})
        }
        console.warn(error)
      }
    },

    async obtener_data({commit, state, dispatch}, data){
      try {
        const config = {
          method: 'get',
          url: `${state.preferencias.IP}/api/${data.api}`,
          headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${state.token_sesion}`
          }
        }

        let r = await axios(config)
        
        if (r.status == 200) {

          if (r.data.data.length == 0) {
            
            minix({icon: 'info', mensaje: 'NO HAY REGISTROS', tiempo: 3000})
            return []

          }else{
            return r.data.data
          }

        }else{
          return []
        }

      } catch (error) {

        if (error.response.status == 401 && error.response.data.error.name == 'UnauthorizedError') {
          
          showAlert('La sesión ha caducado', function(){
            commit('set_load_tiempo', true)
            localStorage.removeItem('rhomio')
  
            setTimeout(() => {
                router.replace('Login')
            }, 1000);
          })

        }else{
          console.log(error)
        }
        
      }
    },

    async actualizar_data({commit, state, dispatch}, data){
      try {
        const config = {
          method: 'put',
          url: `${state.preferencias.IP}/api/${data.api}`,
          headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${state.token_sesion}`
          },
          data: data.p == 'actualiza_usuarios' ? data.formulario : { data: data.formulario }
        }

        const r = await axios(config)

        if (r.status == 200) {
          if (data.alerta) {
            minix({icon: 'success', mensaje: data.alerta, tiempo: 3000})
          }else{
            minix({icon: 'success', mensaje: 'ACTUALIZADO :)', tiempo: 3000})
          }
        }else{
          minix({icon: 'info', mensaje: 'HUBO UN ERROR AL ACTUALIZAR', tiempo: 6000})
          console.log(r.data)
        }

      } catch (error) {
        console.log(error)
      }
    },

    async borrar_data({commit, state, dispatch}, data){
      try {

        const config = {
          method: 'delete',
          url: `${state.preferencias.IP}/api/${data.api}`,
          headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${state.token_sesion}`
          }
        }

    
        const r = await axios(config)

        if (r.status == 200) {
          minix({icon: 'success', mensaje: 'BORRADO :)', tiempo: 3000})
          return true
        }else{
          minix({icon: 'info', mensaje: 'HUBO UN ERROR AL BORRAR', tiempo: 6000})
          console.log(r.data)
          return false
        }
          
       

      } catch (error) {
        console.log(error)
      }
    },

    // DATOS USUARIO

    async obtener_datos_usuario({commit, state}){
      try {
        
        const config = {
          method: 'get',
          url: `${state.preferencias.IP}/api/users/me?populate=*`,
          headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${state.token_sesion}`
          }
        }

        let r = await axios(config)

        if (r.status == 200) {
          commit('set_data_usuario', r.data)
          commit('set_role', r.data.role_sistema)
        }

      } catch (e) {
        console.log(e)
      }
    },

    // CERRAR SESIÓN

    cerrar_sesion({commit, state}){
      pregunta({titulo: 'Seguro que deseas salir?', texto: 'Está a punto de salir del sistema', afirmacion: 'Si, salir!'}, async (i) =>{

        
        if (i) {
          commit('set_load_tiempo', true)
          localStorage.removeItem('rhomio')

              setTimeout(() => {
                  router.replace('Login')
              }, 1000);


              // let b = await axios.post(`${IP}:${PUERTO}/api/login/cerrarsesion`, f, this.$store.state.token)
              // console.log(b.data)
          }else{
            commit('set_load_tiempo', false)
          }
      })

    },

    //INICIA LA CONEXIÓN SOCKET.IO AL SERVIDOR

    async conexion_socket({commit, state, dispatch}){
      const SERVER_URL = `${state.preferencias.IP}`
      const JWT_TOKEN = state.token_sesion
      const socket = io(SERVER_URL, { 
        auth:{
          token: JWT_TOKEN
        },
        transports : ['websocket']
      })

      socket.on('connect', ()=>{
        socket.on('message:bienvenida', (data)=>{
          console.log(data)
        })

        socket.on('action:update', (data)=>{

          if (data.api == 'registro/c/pendientes') {
            if (state.role == 'administrador') {
              dispatch('actualizar_modulo', data)
            }
          }else{
            dispatch('actualizar_modulo', data)
          }

        })
      })

    },
    // ACTUALIZA LOS MODULOS QUE VIENEN POR SOCKET

    async actualizar_modulo({commit, state, dispatch}, modulo){

      const config = {
        method: 'get',
        url: `${state.preferencias.IP}/api/${modulo.api}`,
        headers: {
          'Content-Type': 'application/json',
          'Authorization': `Bearer ${state.token_sesion}`
        }
      }

      const d = await axios(config)
      commit(modulo.set, d.data.data)

      
    },

    // DESCARGA LOS DATOS PREDEFINIDIOS COMO: CATEGORIAS, USUARIOS, PERMISOS, ETC

    async descargar_datos({commit, state, dispatch}){ 
      try {

        if (state.rutas.length != 0) {
          
          for (let i = 0; i < state.rutas.length; i++) {
            const e = state.rutas[i];
          
    
            const config = {
              method: 'get',
              url: `${state.preferencias.IP}/api/${e.api}`,
              headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${state.token_sesion}`
              }
            }
            
     
            const d = await axios(config)
            
            if (d.status == 200) {
              commit(e.set, d.data.data)
            }
            
         }

        }else{
          console.log('## No hay datos para descargar.... ##')
        }
        
      } catch (error) {
      
         console.error(error)
         
        //  if (error.request.status == 403) {
        //   if (state.role == 'administrador') {
        //     minix({icon: 'error', mensaje: 'Un error ha ocurrido, revisa la consola :)', tiempo: 3000})
        //   }
        //  }

      }
      
    }
  },
  plugins: [vuexPersist.plugin],
  modules: {
  }
})
