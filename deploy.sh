#!/bin/bash

echo '[+] COMPILANDO APP'
echo ''

npm run build

echo ''
echo '[~] REALIZANDO CAMBIOS NECESARIOS'

mv dist pc

echo ''
echo '[-] BORRANDO ARCHIVOS REMOTOS'

ssh root@arub4.ddns.net 'rm -rf /var/www/html/pc/*'

echo ''
echo '[+] SUBIENDO DATOS AL SERVIDOR'

scp -r pc root@arub4.ddns.net:/var/www/html/

echo ''
echo '[-] BORRANDO ARCHVOS LOCALES...'

rm -rf pc

echo ''
echo ''
echo '[******] TODO LISTO'
